# Very simple Makefile only (no use of variables, all hardcoded)
#
# Make sure nothing is running on local port 8080 (for sake of time, hardcoded)

unit-tests: ## Run unit tests
	go test -v ./...

build: ## Build the container
	docker build -f docker_files/Dockerfile -t echo-server .

build-nc: ## Build the container without caching
	docker build --no-cache -f docker_files/Dockerfile -t echo-server .

run-no-tls: ## Run container on port configured in `config.env`
	docker run --name echo-server --rm -d -p 8080:8080 echo-server

create-self-signed-cert: ## Create a self-signed certficate and key in local `tls` dir
	openssl req -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -out tls/cert.crt -keyout tls/the.key -subj "/CN=www.example.com"

run-with-tls:  ## Run HTTP 2.0 server with TLS using certificate/key found in `tls` dir
	docker run --name echo-server --rm -d -p 8080:8080 -v ${CURDIR}/tls:/tls -e CERTFILE=/tls/cert.crt -e KEYFILE=/tls/the.key echo-server

stop: ## Stop any running echo-server
	docker stop echo-server

curl-no-tls:  ## Send test curls with sample bodies to HTTP 1.1 (no TLS) server
	@echo "Simple string body echoed:"
	@echo 
	curl -u user:pass -d 'SAMPLE BODY' -X POST 'http://localhost:8080/api/v1/echo'
	@echo
	@echo
	@echo "Json body as string echoed:"
	@echo
	curl -u user:pass -d '{Another Sample Body}' -X POST 'http://localhost:8080/api/v1/echo'
	@echo
	@echo
	@echo "Checking headers only, notice HTTP 1.1 protocol:"	
	@echo
	curl -u user:pass -I -X POST 'http://localhost:8080/api/v1/echo'
	@echo 

curl-tls-insecure:  ## Send test curls with sample bodies to HTTP 2.0 (with TLS) server without cert checking
	@echo "Simple string body echoed:"
	@echo 
	curl -k -u user:pass -d 'SOME SAMPLE BODY' -X POST 'https://localhost:8080/api/v1/echo'
	@echo
	@echo
	@echo "Json body as string echoed:"
	@echo
	curl -k -u user:pass -d '{Json body}' -X POST 'https://localhost:8080/api/v1/echo'
	@echo
	@echo
	@echo "Checking headers only, notice HTTP 2.0 protocol:"	
	@echo
	curl -k -u user:pass -I -X POST 'https://localhost:8080/api/v1/echo'
	@echo 


curl-tls:  ## Send test curls with sample bodies to HTTP 2.0 (with TLS) server WITH cert checking
	@echo "Simple string body echoed:"
	@echo 
	curl -u user:pass -d 'SOME SAMPLE BODY' -X POST 'https://localhost:8080/api/v1/echo'
	@echo
	@echo
	@echo "Json body as string echoed:"
	@echo
	curl -u user:pass -d '{Json body}' -X POST 'https://localhost:8080/api/v1/echo'
	@echo
	@echo
	@echo "Checking headers only, notice HTTP 2.0 protocol:"	
	@echo
	curl -u user:pass -I -X POST 'https://localhost:8080/api/v1/echo'
	@echo 
