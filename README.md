# Echo Server

HTTP server that echoes back whatever body client sent via request.  
This is wrapped in Docker container image for easy build and deployment.  

## API Docs
Consult the OpenAPI 3.0 documentation for API in [this folder](openapi_specs) of project root.  
You can copy paste spec yaml files into [Swagger Editor](https://editor.swagger.io/) to interactively view the documentation.  

## Support for Concurrent Clients
This server is written in Go and will default to running as many kernel-level threads  
as there are cores detected on the system.  Thereafter, as many HTTP request-serving  
light-weight go-routines will be run on these to serve concurrent clients.  Thus concurrency  
is achieved. If you want to set a different number of kernel-level threads, please  
use the GOMAXPROCS environment variable in your docker run.  Note, this does not  
affect the server's ability to service multiple requests via go-routines, though  
it may affect the absolute overall concurrency upper-limit in the usual way.   
This is hardly important for our simple exercise where the server just echoes requests back.  

## Versioning
Currently the git repo does not have any special branching/merging workflow in mind or any  
version tags for the overall echo server to speak of.  
Nevertheless, actual API is semantically versioned in OpenAPI 3.0 spec (i.e. info: > version:). 
Further, major API version number is used in the URL path(s) of the APIs to allow future  
cases where we might want to support breaking changes while allowing clients time to switch over.  

E.g.,  
- 1.X.X API versions will be supported by API paths `/api/v1/*`  
- 2.X.X API versions will switch to API paths `/api/v2/*` 

This allows for a couple releases while we transition clients across the breaking changes.  

## Pre-Requisite Local Tools
1. go version 1.15+  (only needed for unit tests)
2. docker version 18+ 
3. make version 4+

Though, if you have somewhat recent versions for these, things just might work.  

## Basic Access Authentication
HTTP server has a simple version of basic access authentication enabled always.  
The username and password are hardcoded to `user` and `pass`.   
This is just for illustration purposes, usually there is a user database or identity provider  
to check these credentials against.  Alternatively, there are many more advanced (richer)  
authenatication mechanisms available like Oauth 2.0 with OpenID connect, API keys, etc. 

## Simple Makefile For Convenience
A lot of the commands explained in detail below can be executed in project root  
using the [Makefile](Makefile). 
`cat Makefile` shows documentation for each target. You run target you want like `make TARGETNAME`  

For example, this sequence of make commands can showcase an end-to-end demo:

```
make unit-tests                 # Run code unit tests
make build                      # Build container
make create-self-signed-cert    # Create self signec x.509 cert and private key
make run-with-tls               # Run echo server with TLS (and HTTP 2.0) enabled
make curl-tls-insecure          # Run few curl test cases on the echo server
make stop                       # Stop the echo server
```

Note: Before using these make targets, make sure nothing is running on port 8080 locally.  
      Later, Makefile will be enhanced to pull in configs like port from a separate file  
      that has config variables or config environment variables in it.  

## HTTPS/TLS Support
If you intend to enable TLS (i.e., HTTPS), which automatically also switches server protocol  
from HTTP 1.1 to HTTP 2.0 in the server, you need to provide x.509 certificate and key in  
the docker run command (see later).  
HTTPS is required if you want to prevent the basic auth credentials mentioned above from being  
sent via cleartext to server. A client-trusted certificate also authenticates our server with  
client during connection establishment (i.e., so client trusts it is connecting the correctd server).  

If the intention is to relax the need for this trust but still require an encrypted channel,  
one can use self-signed certificates to start this HTTP server. 

An easy way to create one would be using a command like this from project root:

```
openssl req -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -out tls/cert.crt -keyout tls/the.key -subj "/CN=www.example.com"
```

Note: If a self-signed certficate is used, a client must turn off the associated check. For example,  
      the `-k` flag of curl does this. This does not disable encryptiion and message integrity,  
      those guarantees should still exist. 

Note2: Above sample comment puts x.509 certificate and private key in the project's `tls` directory.  
       This directory's contents are not tracked by git.  You can pick a different location  
       instead as you please.  

### To Run Golang Unit Tests For Server
From project root run:  
```
go test -v ./...
```

### Build Server Container from Project Root
```
docker build -f docker_files/Dockerfile -t echo-server .
```

### Run HTTP 1.1 Server Without TLS
To run an HTTP 1.1 Server without TLS, invoke your docker container after building like this:  
```
docker run --name echo-server --rm -d -p 8080:8080 echo-server 
```
If you want to run your server on a different local port, change `-p 8080:8080` to `-p ANOTHER_PORT:8080`  

### Run HTTP 2.0 Server With TLS (i.e., HTTPS)
To run an HTTP 2.0 server with TLS enabled, invoke your docker container after buildilng like this:
```
docker run --name echo-server --rm -d -p 8080:8080 -v ABS_PATH_PROJECT_ROOT/tls:/tls -e CERTFILE=/tls/cert.crt -e KEYFILE=/tls/the.key echo-server 
```
If you want to run your server on a different local port, change `-p 8080:8080` to `-p ANOTHER_PORT:8080`  

Note: Above invocation assumes you have x.509 certificate and key in the project's `tls` directory.  
      Modify this as necessary.  

### Test HTTP 1.1 (no TLS) Server Running Locally With Curl
```
curl -u user:pass -d 'SOME SAMPLE BODY' -X POST 'http://localhost:8080/api/v1/echo'
```

### Test HTTP 2.0 (with TLS) HTTPS Server Running Locally With Curl
```
curl -k -u user:pass -d 'SOME SAMPLE BODY' -X POST 'https://localhost:8080/api/v1/echo'
```

Note: You do not need `-k` to curl if your local server has been set up to trust the certificate  
      that you provided the docker run command.  

### Stop Echo Server Container
```
docker stop echo-server
```

### Other Docker Operations
Please see your nearest Docker documentation or command-line help. 


