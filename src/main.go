/*
* Echo Server
*
* HTTP(s) server that echoes back request body to client
*
* API version: 1.0.0
 */
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// Completely for sake of time, hardcode the only
// acceptable basic auth credentials (username and
// passsword) here
const (
	authorizedUser = "user"
	authorizedPass = "pass"
)

// checkBasicAuth checks that basic auth user/pass provided in
// request against the expected user and pass
func checkBasicAuth(r *http.Request, expUser, expPass string) bool {
	user, pass, ok := r.BasicAuth()
	if !ok {
		return false
	}
	return user == expUser && pass == expPass
}

// echoHandler will echo back request body as response body.
// Note: This is supposed to be quick exercise, so nowhere are we checking that
//       request method is POST nor do we set headers for CORS or any specific headers
//       for that matter (though some default headers will be set for us automatically)
func echoHandler(w http.ResponseWriter, r *http.Request) {
	if !checkBasicAuth(r, authorizedUser, authorizedPass) {
		w.Header().Set("WWW-Authenticate", `Basic realm="echo-server"`)
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, fmt.Sprint("Error reading body: ", err.Error()), http.StatusBadRequest)
		return
	}
	fmt.Fprint(w, string(b))
}

func main() {
	// Get location of x509 encryption certificate and key for TLS from env vars
	// Having these two will also enable HTTPS 2.0 (instead of 1.1)
	certFile := os.Getenv("CERTFILE")
	keyFile := os.Getenv("KEYFILE")

	// Fix port as docker run can always re-map
	serverPort := ":8080"

	log.Println("About to start server listening on port " + serverPort + "...")

	http.HandleFunc("/api/v1/echo", echoHandler)

	if certFile != "" && keyFile != "" {
		log.Println("HTTP/2 Server started with TLS enabled (HTTPS)")
		err := http.ListenAndServeTLS(serverPort, certFile, keyFile, nil)
		log.Fatal(err)
	} else {
		log.Println("HTTP/1 Server started")
		err := http.ListenAndServe(serverPort, nil)
		log.Fatal(err)
	}
}
