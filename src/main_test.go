package main

import (
	"bytes"
	"encoding/base64"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

// Test echoHandler returns Unauthorized (401) when basic auth fails
func TestEchoHandlerUnauthorized(t *testing.T) {

	// Create some URL with path /api/v1/echo
	url := "http://a.b.c/api/v1/echo"

	// Create a POST request and body that causes an error on reading
	req, err := http.NewRequest("POST", url, errReader(0))
	if err != nil {
		t.Fatal(err)
	}
	// Note: No "Authorization: " header is set on request

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(echoHandler)

	// Call handler ServeHTTP method directly and pass Request, ResponseRecorder
	handler.ServeHTTP(rr, req)

	// Check the status code is unauthorized
	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("Handler returned wrong http status code: got [%v] wanted [%v]",
			status, http.StatusOK)
	}

	// Check the response body has the error
	bd, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}
	expectedRespStr := "Unauthorized\n"
	if string(bd) != expectedRespStr {
		t.Errorf("Handler returned unexpected body: got [%v] wanted [%v]",
			string(bd), expectedRespStr)
	}

	// Check that response has WWW-Authenticate header with the dummy basic realm we set
	wwwAuthHeader := rr.Header().Get("WWW-Authenticate")
	expWwwAuthHeader := `Basic realm="echo-server"`
	if wwwAuthHeader != expWwwAuthHeader {
		t.Errorf("Handler returned unexpected WWW-Authenticate header: got [%v] wanted [%v]",
			wwwAuthHeader, expWwwAuthHeader)
	}
}

// encodeBasicAuthCredentials is helper function that encodes username:password
// fields using base64 encoding (which will be used within Authorization: header
// test requests)
func encodeBasicAuthCredentials(username, password string) string {
	creds := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(creds))
}

// errReader will be an io.Reader that returns an error
// with error string "test error".
// We will pass this as the body of a request to simulate
// an error when handler reads body.
type errReader int

func (errReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("Some error")
}

// Test echoHandler getting error while reading request body
func TestEchoHandlerUnableReadBody(t *testing.T) {

	// Create some URL with path /api/v1/echo
	url := "http://a.b.c/api/v1/echo"

	// Create a POST request and body that causes an error on reading
	req, err := http.NewRequest("POST", url, errReader(0))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization",
		"Basic "+encodeBasicAuthCredentials(authorizedUser, authorizedPass))

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(echoHandler)

	// Call handler ServeHTTP method directly and pass Request, ResponseRecorder
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Handler returned wrong http status code: got [%v] wanted [%v]",
			status, http.StatusOK)
	}

	// Check the response body has the error
	bd, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}
	expectedRespStr := "Error reading body: Some error\n"
	if string(bd) != expectedRespStr {
		t.Errorf("Handler returned unexpected body: got [%v] wanted [%v]",
			string(bd), expectedRespStr)
	}
}

// Test echoHandler succeeds to echo body if it can be read
func TestEchoHandler(t *testing.T) {

	// Create some URL with path /api/v1/echo
	url := "http://a.b.c/api/v1/echo"

	// Create a POST request and sample body to test with
	var bodyStr = `{"body":"rest of body"}`
	var body = bytes.NewBuffer([]byte(bodyStr))
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization",
		"Basic "+encodeBasicAuthCredentials(authorizedUser, authorizedPass))

	// Create a ResponseRecorder to record the response
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(echoHandler)

	// Call handler ServeHTTP method directly and pass Request, ResponseRecorder
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong http status code: got [%v] wanted [%v]",
			status, http.StatusOK)
	}

	// Check the response body is what we expect
	bytesRespBody, err := ioutil.ReadAll(rr.Body)
	if err != nil {
		t.Fatal(err)
	}
	respBody := string(bytesRespBody)
	if respBody != bodyStr {
		t.Errorf("Handler returned unexpected body: got [%v] wanted [%v]",
			respBody, bodyStr)
	}
}
